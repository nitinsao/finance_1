#include <stdio.h>
#include <stdlib.h>
#include <limits.h> 
#include <string.h>
#include <gmp.h>
#include <time.h>
#define MAX 100000000//0

typedef struct integer
{
	char integer_str[202]; // first character is sign. if it is negative, sign character will be '-'.
	int total_digit;
	int precision;
}Var;

Var* addition(const Var *a, const Var *b)
{
	mpz_t x,y,z;
	Var *c = (Var *)malloc(sizeof(Var));
	mpz_init (x);
	mpz_init (y);
	mpz_init (z);
	mpz_set_str (x, a->integer_str, 10);
	mpz_set_str (y, b->integer_str, 10);
	mpz_add (z, x, y);
	mpz_get_str (c->integer_str,10,z);
	int len = strlen(c->integer_str);
	if( (c->integer_str[0] == '-' && len > 101) || len > 100)
	{
		printf("Overflow!!!\n");
		return NULL;
	}
	c->total_digit = len;
	c->precision = 50;
	mpz_clears(x,y,z,NULL);
	return c;
}

Var* subtraction(const Var* a,const Var* b)
{
	Var *c = (Var *)malloc(sizeof(Var));
	mpz_t x,y,z;
	mpz_init (x);
	mpz_init (y);
	mpz_init (z);
	mpz_set_str (x, a->integer_str, 10);
	mpz_set_str (y, b->integer_str, 10);
	mpz_sub (z, x, y);
	mpz_get_str (c->integer_str,10,z);
	int len = strlen(c->integer_str);
	if( (c->integer_str[0] == '-' && len > 101) || len > 100)
	{
		printf("Overflow!!!\n");
		return NULL;
	}
	c->total_digit = len;
	c->precision = 50;
	mpz_clears(x,y,z,NULL);
	return c;
}
Var* multiplication(const Var* a,const Var* b)
{
	Var *c = (Var *)malloc(sizeof(Var));
	mpz_t x,y,z;
	mpz_init (x);
	mpz_init (y);
	mpz_init (z);
	mpz_set_str (x, a->integer_str, 10);
	mpz_set_str (y, b->integer_str, 10);
	mpz_mul (z, x, y);
	mpz_get_str (c->integer_str,10,z);
	int len = strlen(c->integer_str);
	if( (c->integer_str[0] == '-' && len > 151) || len > 150)
	{
		printf("Overflow!!!\n");
		return NULL;
	}
	len -= 50;
	c->integer_str[len] = '\0';
	c->total_digit = len;
	c->precision = 50;
	mpz_clears(x,y,z,NULL);
	return c;
}
Var* division(const Var* a,const Var* b)
{
	Var *c = (Var *)malloc(sizeof(Var));
	mpz_t x,y,z;
	mpz_init (x);
	mpz_init (y);
	mpz_init (z);
	mpz_set_str (x, a->integer_str, 10);
	mpz_set_str (y, b->integer_str, 10);
	mpz_cdiv_q (z, x, y);
	mpz_get_str (c->integer_str,10,z);
	int len = strlen(c->integer_str);
	c->precision = 50;
	mpz_clears(x,y,z,NULL);
	return c;
}
Var* exponentiate(const Var* base,const Var* exp)
{
	return NULL;
}

int main(int argc, char const *argv[])
{
	Var *test1,*test2,*c;
	FILE *p,*a,*s,*m,*o;
	o = fopen("timing_status.out","w");
	test1 = (Var *)malloc(sizeof(Var));
	test2 = (Var *)malloc(sizeof(Var));
	clock_t t;

	// ==========================ADDITION================================
	p = fopen("input.in","r");
	t = clock();
	a = fopen("addition.out","w");
	for(int i=0 ; i<MAX*2 ; i+=2)
	{
		fscanf(p,"%s %d %s %d",test1->integer_str,&test1->total_digit,test2->integer_str,&test2->total_digit);
		c = addition(test1,test2);
		fprintf(a,"%s\n",c->integer_str);
		free(c);
	}
	fprintf(o,"time taken for addition is : %.10lf\n",(((double)(clock()-t))/CLOCKS_PER_SEC)/MAX);
	fclose(a);
	fclose(p);


	//=========================subtraction===============================
	p = fopen("input.in","r");
	t = clock();
	s = fopen("subtraction.out","w");
	for(int i=0 ; i<MAX*2 ; i+=2)
	{
		fscanf(p,"%s %d %s %d",test1->integer_str,&test1->total_digit,test2->integer_str,&test2->total_digit);
		c = subtraction(test1,test2);
		fprintf(s,"%s\n",c->integer_str);
		free(c);
	}
	fprintf(o,"time taken for subtraction is : %.10lf\n",(((double)(clock()-t))/CLOCKS_PER_SEC)/MAX);
	fclose(s);
	fclose(p);


	//=======================multiplication==============================
	p = fopen("input.in","r");
	t = clock();
	m = fopen("multiplication.out","w");
	for(int i=0 ; i<MAX*2 ; i+=2)
	{
		fscanf(p,"%s %d %s %d",test1->integer_str,&test1->total_digit,test2->integer_str,&test2->total_digit);
		c = multiplication(test1,test2);
		fprintf(m,"%s\n",c->integer_str);
		free(c);
	}
	fprintf(o,"time taken for multiplication is : %.10lf\n",(((double)(clock()-t))/CLOCKS_PER_SEC)/MAX);
	fclose(p);
	fclose(m);
	fclose(o);
	return 0;
}
